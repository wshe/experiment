class E2ETest(object):
    """End to end test."""

    @staticmethod
    def test():
        print('Hello, this is an end-to-end test!')


if __name__ == '__main__':
    E2ETest().test()
