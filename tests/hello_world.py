class HelloWorldTest(object):
    """Hello world test."""

    @staticmethod
    def test():
        print('Hello, this is Wei She!')


if __name__ == '__main__':
    HelloWorldTest().test()
